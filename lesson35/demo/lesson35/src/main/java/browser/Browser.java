package browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import reporting.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Main on 12.04.2017.
 */
public class Browser {
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 15;
    private static final int WAIT_ELEMENT_TIMEOUT = 10;

    private WebDriver driver;
    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    private static Browser init() {
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver","src\\main\\resources\\geckodriver.exe");
        driver = getChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }

    private static WebDriver getFireFoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
//        profile.setPreference("browser.download.dir", GenerateDataUtils.getDownloadDirectoryPath());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        return new FirefoxDriver(profile);
    }

    private static WebDriver getRemoteDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.downloadFileTest.folderList", 2);
        profile.setPreference("browser.downloadFileTest.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);

        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }

    private static WebDriver getChromeDriver() {
        Map<String, Object> chromeOptions = new Hashtable<String, Object>();
        chromeOptions.put("profile.default_content_settings.popups", 0);
        chromeOptions.put("download.prompt_for_download", "false");
//        chromeOptions.put("download.default_directory", GenerateDataUtils.getDownloadDirectoryPath());
        chromeOptions.put("download.directory_upgrade", true);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("chromeOptions", chromeOptions);
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        return new ChromeDriver();
    }

    public void open(String url) {
        Logger.info("Going to URL: " + url);
        driver.get(url);
    }

    public static void kill() {
        if (instance != null) {
            try {
                instance.driver.quit();
            } finally {
                instance = null;
            }
        }
    }

    public void waitForElementPresent(By locator) {
//        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

    public void waitForElementEnabled(By locator) {
//        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForElementVisible(By locator) {
//        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

//    public void highlightElement(By locator) {
//        ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='5px solid green'", driver.findElement(locator));
//    }

    public void click(final By locator) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
//        highlightElement(locator);
        Logger.info("Clicking element '" + driver.findElement(locator).getText() + "' (Located: " + locator + ")");
        driver.findElement(locator).click();
    }

    public void type(final By locator, String text) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
//        highlightElement(locator);
        Logger.info("Typing text '" + text + "' to input form '" + driver.findElement(locator).getAttribute("name") + "' (Located: " + locator + ")");
        driver.findElement(locator).sendKeys(text);
    }

    public String read(final By locator) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
//        highlightElement(locator);
        Logger.info("Reading text: " + driver.findElement(locator).getText());
//        takeScreenshot();
//        unHighlightElement(locator);
        return driver.findElement(locator).getText();
    }

    public boolean isDisplayed(By locator) {
        waitForElementPresent(locator);
        boolean succeed = driver.findElements(locator).size() > 0;
        if (succeed) {
            Logger.info("Element " + driver.findElement(locator).getText() + " is present.");
//            highlightElement(locator);
//            takeScreenshot();
//            unHighlightElement(locator);
        } else Logger.warn(("Element " + driver.findElement(locator).getText() + " is not present."));
        return succeed;
    }

    public void refresh() {
        driver.navigate().refresh();
    }
}
