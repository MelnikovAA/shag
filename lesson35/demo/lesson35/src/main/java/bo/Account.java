package bo;

/**
 * Created by Vel2 on 13.04.2017.
 */
public class Account {

    private String Login;
    private String Password;

    public Account (String login, String password) {
        Login = login;
        Password = password;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }
}
