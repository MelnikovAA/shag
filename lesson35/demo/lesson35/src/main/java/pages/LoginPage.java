package pages;

import bo.Account;
import org.openqa.selenium.By;

/**
 * Created by Account on 05.04.2017.
 */
public class LoginPage extends AbstractPage {

    private static final String BASE_URL = "https://mail.yandex.ru";
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[contains(@class,'new-auth-submit')]/*[@type=\"submit\"]");

    public LoginPage open() {
        browser.open(BASE_URL);
        return this;
    }


    public LoginPage inputLogin(String login) {
        browser.type(LOGIN_INPUT_LOCATOR,login);
        return this;
    }

    public LoginPage inputPassword(String password) {
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        return this;
    }

    public InboxPage clickLoginButton() {
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new InboxPage();
    }

    public LoginPage loginWithAccount(Account account) {
        browser.type(LOGIN_INPUT_LOCATOR, account.getLogin());
        browser.type(PASSWORD_INPUT_LOCATOR, account.getPassword());
        return this;
    }
}
