package pages;

import bo.Letter;
import org.openqa.selenium.By;

/**
 * Created by Main on 10.04.2017.
 */
public class ComposePage extends BasePage {

    private static final By RECEPIENT_INPUT_LOCATOR = By.xpath("//*[@name='to']");
    private static final By SUBJECT_INPUT_LOCATOR = By.xpath("//*[@name='subj']");
    private static final By BODY_INPUT_LOCATOR = By.xpath("//*[@role='textbox']");
    private static final By SEND_BUTTON_LOCATOR = By.cssSelector(".js-send");
    private static final By CLOSE_POPUP_LOCATOR = By.cssSelector("button. nb-button _nb-small-action-button _init nb-with-s-right-gap js-resolve");
    private static final By POPUP_LOCATOR = By.cssSelector("div._nb-popup-i");

    public ComposePage sendLetter (Letter letter) {
        browser.type(RECEPIENT_INPUT_LOCATOR, letter.getRecipient());
        browser.type(SUBJECT_INPUT_LOCATOR, letter.getSubject());
        browser.type(BODY_INPUT_LOCATOR, letter.getBody());
        browser.click(SEND_BUTTON_LOCATOR);
        return this;
    }

    public ComposePage confirmPopupIfDisplayed() {
        if (browser.isDisplayed(POPUP_LOCATOR)) {
            browser.click(CLOSE_POPUP_LOCATOR);
        }
        return this;
    }
}
