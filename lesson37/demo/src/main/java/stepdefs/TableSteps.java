package stepdefs;

import bo.Table;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

/**
 * Created by Main on 19.04.2017.
 */
public class TableSteps {
    public static final int FALLING_APPLES = 2;
    Table table;

    @Given("^A table with (\\d+) apples$")
    public void createTableWithApples(int applesWas){
        table = new Table();
        table.setApples(applesWas);
    }

    @When("^I kick the table(?: again|)$")
    public void iKickTheTable()  {
        table.setApples(table.getApples() - FALLING_APPLES);
    }

    @Then("^(\\d+) apples are on table$")
    public void apples_leftApplesAreOnTable(int applesLeft) {
        Assert.assertEquals(applesLeft, table.getApples(), "Table contain not " + applesLeft + " apples!");
    }
}
