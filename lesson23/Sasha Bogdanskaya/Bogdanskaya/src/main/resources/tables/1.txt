| brand | model | body      | year | color | power | engine volume | transmission | fuel   |
--------------------------------------------------------------------------------------------
| audi  | TT    | sport     | 2000 | red   | 250   | 4.5           | auto         | petrol |
| BMW   | x5    | crossover | 2005 | black | 200   | 3.5           | auto         | diesel |
| opel  | astra | sedan     | 1993 | white | 140   | 2.5           | mechanic     | diesel |