import java.util.Arrays;

/**
 * Created by Vel2 on 26.03.2017.
 */
public class Methods {

    public static void firstMethod (String word1, String word2) {

        char[] array1 = word1.toCharArray();
        Arrays.sort(array1);
        char[] array2 = word2.toCharArray();
        Arrays.sort(array2);
        int k = 0;

        if (array1.length == array2.length) {
            for (int i = 0; i < array1.length; i++) {
                if (array1[i] == array2[i]) {
                    k++;
                }
            }
            if (k == array1.length) {
                System.out.println("This words are the anagram!");
            } else {
                System.out.println("This words are not the anagram!");
            }
        } else {
            System.out.println("This words are not the anagram!");
        }

    }

    public static void secondMethod (String word1, String word2) {

    }
}
