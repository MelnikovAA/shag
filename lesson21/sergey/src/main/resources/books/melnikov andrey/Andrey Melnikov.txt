﻿1. Иерусалимский покер, Э. Уитмор, 1978;
2. Старик и море,  Э.Хемингуе, 1960;
3. Девушка с татуировкой дракона, С.Ларсон, 1995;
4. Архипелаг Гулаг,  А.Солженицен, 1960;
5. Колосья под серпом твоим, В.Короткевич, 1965;
6. «Гамлет» Уильяма Шекспира, 1760;
7. «Евгений Онегин», Александра Пушкина, 1880;
8. «Портрет Дориана Грея», Оскара Уайльда, 1950
9. «Превращение», Франца Кафки, 1951;
10. «Над пропастью во ржи», Джерома Дэвида Сэлиндже 1935;
