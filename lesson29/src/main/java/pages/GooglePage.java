package pages;

import org.openqa.selenium.By;

public class GooglePage extends AbstractPage{
    private static final By SEARCH_INPUT_LOCATOR = By.xpath("//*[@name='q']");
    private static final By SEARCH_BUTTON_LOCATOR = By.xpath("//*[@id='_fZl']");
    private static final By YOUTUBE_LINK_LOCATOR = By.xpath("//a[contains(text(),'Full Song') and contains(@href,'youtube.com')]");
//    private static final By I_AM_LUCKY_BUTTON_LOCATOR = By.xpath("//*[@name='btnI']");

    public GooglePage() {
        super();
    }

    public GooglePage open(){
        driver.get("https://google.com");
        return this;
    }

    public GooglePage inputSearchText(String text) {
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys(text);
        return this;
    }

    public GooglePage clickSearchButton(){
        AbstractPage.driver.findElement(SEARCH_BUTTON_LOCATOR).click();
        return this;
    }

    public boolean isFullSongLinkPresent() {
        return driver.findElement(YOUTUBE_LINK_LOCATOR).isDisplayed();
    }
}
