package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.GooglePage;
import pages.YandexInboxPage;
import pages.YandexLoginPage;

/**
 * Created by User on 05.04.2017.
 */
public class ShagTests {

    @Test(description = "google test")
    public void googleTest(){
        GooglePage googlePage = new GooglePage();
        googlePage.open().inputSearchText("banana song").clickSearchButton();
        Assert.assertTrue(googlePage.isFullSongLinkPresent(), "Full song link is not present");
        googlePage.close();
    }

    @Test(description = "Yandex login test")
    public void yandexLoginTest(){
        YandexInboxPage inboxPage = new YandexLoginPage().open().inputLogin("shagtest").inputPassword("shagtest123").clickLoginButton();
        Assert.assertTrue(inboxPage.isLoginNameDisplayed());
    }

    /*
    * Hometask
    * написать такие же страницы и тесты для отправления письма (самому себе) и проверить его наличие во входищих
    * */
}
