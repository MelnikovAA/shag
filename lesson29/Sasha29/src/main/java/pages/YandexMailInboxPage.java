package pages;

import org.openqa.selenium.By;


public class YandexMailInboxPage extends AbstractPage {

    private static final By LOGIN_NAME_LOCATOR = By.xpath("//div[@class='mail-User-Name']");
    private static final By WRITE_LETTER_LOCATOR = By.xpath("//a[@title='Написать (w, c)']");

    public boolean isLoginNameDisplayed() {
        return driver.findElement(LOGIN_NAME_LOCATOR).isDisplayed();
    }

    public YandexMailComposePage clickNewLetterButton() {
        driver.findElement(WRITE_LETTER_LOCATOR).click();
        return new YandexMailComposePage();
    }

}
