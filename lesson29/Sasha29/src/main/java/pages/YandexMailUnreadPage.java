package pages;

import org.openqa.selenium.By;

/**
 * Created by Vel2 on 08.04.2017.
 */
public class YandexMailUnreadPage extends AbstractPage {
    private static final By UNREAD_LETTER_LOCATOR = By.xpath("//div[@class='mail-MessageSnippet-Content']");

    public boolean isUnreadLetterDisplayed() {
        return driver.findElement(UNREAD_LETTER_LOCATOR).isDisplayed();
    }

    public YandexMailLetterPage clickUnreadLetter() {
        driver.findElement(UNREAD_LETTER_LOCATOR).click();
        return new YandexMailLetterPage();
    }
}
