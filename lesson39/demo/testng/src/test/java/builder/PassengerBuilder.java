package builder;


import bo.Passenger;

public class PassengerBuilder {

    public static Passenger createPassenger() {
        String lastname = "Богданская";
        String name = "Александра";
        String passport = "НВ1234567";
        return new Passenger(lastname, name, passport);
    }
}
