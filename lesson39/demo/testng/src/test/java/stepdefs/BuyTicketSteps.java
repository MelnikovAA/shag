package stepdefs;


import bo.Account;
import bo.Passenger;
import bo.TrainRoute;
import builder.AccountBuilder;
import builder.PassengerBuilder;
import builder.TrainRouteBuilder;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import pages.BuyTicketPage;
import pages.HomePage;
import pages.PaymentPage;
import ru.yandex.qatools.allure.annotations.Step;

public class BuyTicketSteps {

    @Step
    @Given("^Login from 'poezd.rw.by'$")
    public void loginByHomePage() {
        Account account = AccountBuilder.createAccount();
        new HomePage().open().openLoginPage().loginByAccount(account).openBuyTicketPage();
    }

    @Step
    @When("^Select train Gomel-Minsk$")
    public void selectRoute() {
        TrainRoute trainRoute = TrainRouteBuilder.createTrainRoute();
        new BuyTicketPage().confirmationRegulations().selectTrainRoute(trainRoute);
    }

    @Step
    @And("^Select date yesterday and seven days$")
    public void selectDate() {
        new BuyTicketPage().selectDate();
    }

    @Step
    @And("^Select travel time less 3 hours$")
    public void selectTravelTime() {
        new BuyTicketPage().selectTravelTime();
    }

    @Step
    @And("^Select train car and place$")
    public void selectTrainCarAndPlace() {
        Passenger passenger = PassengerBuilder.createPassenger();
        new BuyTicketPage().selectTrainCar().sendDataByPassenger(passenger).selectPlaceInTrainCar();
    }

    @Step
    @And("^Go to payment$")
    public void goToPaymentPage() {
        new BuyTicketPage().continueBuyTicket().clickPaymentPage();
    }

    @Step
    @Then("^Open page payment$")
    public void pagePaymentIsDisplayed() {
        PaymentPage paymentPage = new PaymentPage();
        Assert.assertTrue(paymentPage.isDisplayedPaymentPage(), "Payment page not find!");
    }

}

