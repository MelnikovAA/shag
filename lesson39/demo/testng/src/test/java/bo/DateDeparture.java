package bo;


import java.util.Calendar;

public class DateDeparture {

    private Calendar DepartureDate;
    
    public DateDeparture(Calendar date) {
        DepartureDate = date;
    }
    
    public Calendar getDepartureDate() {
        return DepartureDate;
    }
}
