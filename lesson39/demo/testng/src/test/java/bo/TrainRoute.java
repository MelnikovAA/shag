package bo;

/**
 * Created by Vel2 on 20.04.2017.
 */
public class TrainRoute {

    private String StationDeparture;
    private String StationArrival;

    public TrainRoute (String stationDeparture, String stationArrival) {
        StationDeparture = stationDeparture;
        StationArrival = stationArrival;
    }

    public String getStationDeparture() {
        return StationDeparture;
    }

    public String getStationArrival() {
        return StationArrival;
    }
}
