Feature: Buy ticket

    Scenario: Buy train ticket

        Given Login from 'poezd.rw.by'
        When Select train Gomel-Minsk
        And Select date yesterday and seven days
        And Select travel time less 3 hours
        And Select train car and place
        And Go to payment
        Then Open page payment