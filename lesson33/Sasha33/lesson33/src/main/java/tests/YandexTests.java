package tests;

import bo.Account;
import bo.Letter;
import browser.Browser;
import builder.AccountBuider;
import builder.LetterBuilder;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import pages.InboxPage;
import pages.LoginPage;

/**
 * Created by Account on 05.04.2017.
 */
public class YandexTests {

    @Test(description = "Login fail")
    public void loginFailTest() {
        Account account = AccountBuider.createAccount();
        InboxPage inboxPage = new LoginPage().open().sendAccount(account).clickLoginButton();
        Assert.assertTrue(inboxPage.isNotAccountDisplayed());
    }

    @Test(description = "Yandex login test")
    public void yandexLoginTest(){
        InboxPage inboxPage = new LoginPage().open().inputLogin("sashatest2017").inputPassword("sashatest20").clickLoginButton();
        Assert.assertTrue(inboxPage.isLoginNameDisplayed());
    }


    @Test(description = "Send mail test", dependsOnMethods = "yandexLoginTest")
    public void sendMail(){
        Letter letter = LetterBuilder.createLetter();
        InboxPage inboxPage = new InboxPage().clickNewLetter().sendLetter(letter).confirmPopupIfDisplayed().openInbox();
        Assert.assertTrue(inboxPage.isLetterPresent(letter));
    }

    @AfterSuite(description = "Kill browser")
    public void killBrowser(){
        Browser.kill();
    }

    /*
    * Дописать  confirmPopupIfDisplayed()
    * Создать бизнес-объект Аккаунт, создать фабрику аккаунтов (умеет создавать верный и неверный аккаунты)
    * LoginPage должен уметь работать с новым объектом
    * Написать негативный тест на логин (ассерт что сообщение высвечивается)   *
    * */
}
