package bo;

/**
 * Created by Main on 10.04.2017.
 */
public class Letter {
    private String Recipient;
    private String Subject;
    private String Body;

    public Letter(String recipient, String subject, String body) {
        Recipient = recipient;
        Subject = subject;
        Body = body;
    }

    public String getRecipient() {
        return Recipient;
    }

    public String getSubject() {
        return Subject;
    }

    public String getBody() {
        return Body;
    }
}
